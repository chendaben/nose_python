#!/usr/bin/env Python
# -*- coding:utf-8 -*-


'''
测试case
'''

from selenium import webdriver
import time
from unittest import TestCase
import unittest
from htmloutput.htmloutput import HtmlOutput


class testDavdian(unittest.TestCase):
    def setUp(self):
        self.dr = webdriver.Chrome()
        self.dr.get('http://18600967174.davdian.com/login.html?referer=http%3A%2F%2F18600967174.davdian.com%2Fcenter.html')

    def testlogin(self):
        u'正确手机号和密码；账号:18600967174,密码:111111'
        mobile = '18600967174'
        password = '111111'
        self.dr.find_element_by_name('mobile').send_keys(mobile)
        self.dr.find_element_by_name('password').send_keys(password)
        self.dr.find_element_by_class_name('loginbtn').click()
        time.sleep(5)
        self.dr.quit()

    # 判断方法封装
    '''
    def is_login_sucess(self):
        u'判断是否登录成功'
        try:
            pp = self.dr.find_element_by_xpath("/html/body/div/div[2]/div/div[3]")
            print pp
            return True
        except:
            return False
    '''

    def testWrongPwdlogin(self):
        u'输入错误密码；账号:18600967174,密码:aaaaaa'
        mobile = '18600967174'
        password = 'aaaaaa'
        self.dr.find_element_by_name('mobile').send_keys(mobile)
        self.dr.find_element_by_name('password').send_keys(password)
        self.dr.find_element_by_class_name('loginbtn').click()
        pp = self.dr.find_element_by_xpath("/html/body/div/div[2]/div/div[3]")
        '''
        # 判断结果
        result = self.is_login_sucess()
        self.assertTrue(result)
        '''
        self.assertNotEqual(pp,u'密码错误')
        time.sleep(5)
        self.dr.quit()


    def testWrongUserlogin(self):
        u'输入错误手机号；账号:19819819811,密码:111111'
        mobile = '19819819811'
        password = '111111'
        self.dr.find_element_by_name('mobile').send_keys(mobile)
        self.dr.find_element_by_name('password').send_keys(password)
        self.dr.find_element_by_class_name('loginbtn').click()
        pp = self.dr.find_element_by_xpath("/html/body/div/div[2]/div/div[3]")
        self.assertNotEqual(pp,u'手机号错误')
        time.sleep(5)
        self.dr.quit()


    def tearDown(self):
        self.dr.quit()









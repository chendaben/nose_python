#!/usr/bin/env Python
# -*- coding:utf-8 -*-


'''
登录
'''

from selenium import webdriver
import time


# Chrome登录
dr = webdriver.Chrome()

# 设置浏览器大小
dr.set_window_size(400,900)


# 访问首页
fist_url = 'http://18600967174.davdian.com/'
print ("首页地址: %s" %(fist_url))
dr.get(fist_url)
time.sleep(5)

# 访问登录首页
second_url = 'http://18600967174.davdian.com/login.html?referer=http%3A%2F%2F18600967174.davdian.com%2Fcenter.html'
print ("登录页面: %s" %(second_url))
dr.get(second_url)
time.sleep(5)

# 模拟后退首页
print ("back to %s" %(fist_url))
dr.back()
time.sleep(5)

# 模拟前进到登录页
print ("forward to %s" %(second_url))
dr.forward()
time.sleep(5)

# 关闭
dr.quit()




